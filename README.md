Scenariusze testowe: 

1. Test poprawnej rejestracji użytkownika (proszę pamietać, aby użyć unikalnego emaila użykownika by za każdym
wykonaniem testu email był inny).

2. Test niepoprawnej rejestracji użytkownika - hasła w polach „Password” i „Confirm Password” są niezgodne.

3. Test niepoprawnej rejestracji użytkownika - hasło nie spełnia wymagań:
a) nie zawiera wielkiej litery
b) nie zawiera cyfry
c) nie zawiera znaku specjalnego 
4. Test poprawnego logowania.

5. Test menu na stronie (czy kliknięcie w odpowiedni link menu prowadzi nas do odpowiedniego url). 

6. Test poprawnego dodania procesu - sprawdzenie czy został dodany do tabeli Processes.

7. Test poprawnego dodania procesu - sprawdzenie czy wyświetla się na stronie Dashboard.

8. Test niepoprawnego dodania procesu.

9. Test poprawnego dodania charakterystyki do procesu 'DEMO PROJECT' - sprawdzenie czy został dodany do
tabeli Characteristics.

10. Test poprawnego dodania charakterystyki do procesu 'DEMO PROJECT' - sprawdzenie czy wyświetla się się na
stronie Dashboard. 

11. Test niepoprawnego dodania charakterystyki - niewypełnione pole Upper specification limit.

12. Test poprawnego dodania charakterystyki, a nastepnie resultatów do danej charakterystyki i sprawdzenie
raportu – conajmniej czterech wartości z tabeli Results 