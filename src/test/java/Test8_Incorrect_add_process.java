import pages.LoginPage;
import org.testng.annotations.Test;

public class Test8_Incorrect_add_process extends SeleniumBaseTest {
    @Test
    public void incorrectProcess() {
        String shortProcessName = "ab";
        String expErrorMessage = "The field Name must be a string with a minimum length of 3 and a maximum length of 30.";


        new LoginPage(driver)
                .typeEmail("test@test.com")
                .typePassword("Test1!")
                .submitLogin()
                .goToProcesses()
                .clickAddProcess()
                .typeName(shortProcessName)
                .submitCreateWithFailure()
                .assertProcessNameError(expErrorMessage)
                .backToList()
                .assertProcessIsNotShown(shortProcessName);
    }
}

