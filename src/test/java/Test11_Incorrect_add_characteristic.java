import Config.Config;
import pages.LoginPage;
import org.testng.annotations.Test;

import java.util.UUID;

public class Test11_Incorrect_add_characteristic extends SeleniumBaseTest {
    @Test
    public void addCharacteristicNegative() {
        String processName = "DEMO PROJECT";
        String characteristicName = UUID.randomUUID().toString().substring(0, 10);
        String lsl = "8";
        String usl = "";

        new LoginPage(driver)
                .typeEmail(new Config().getApplicationUser())
                .typePassword(new Config().getApplicationPassword())
                .submitLogin()
                .goToCharacteristics()
                    .clickAddCharacteristic()
                    .typeName(characteristicName)
                    .typeLsl(lsl)
                    .typeUsl(usl)
                    .submitCreateWithFailure()
                    .assertUpperError("The value '' is invalid.")
                .backToList()
                    .assertCharacteristicIsNotShown(characteristicName);

    }
}

