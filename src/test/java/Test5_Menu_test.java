import Config.Config;
import org.testng.annotations.Test;
import pages.LoginPage;

public class Test5_Menu_test extends SeleniumBaseTest {
    @Test
    public void menuTest(){
        new LoginPage(driver)
                .typeEmail(new Config().getApplicationUser())
                .typePassword(new Config().getApplicationUser())
                .submitLogin()
                .goToProcesses()
                    .assertProcessesUrl("http://localhost:4444/Projects")
                    .assertProcessesHeader()
                .goToCharacteristics()
                    .assertCharacteristicsUrl("http://localhost:4444/Characteristics")
                .goToDashboard()
                    .assertDashboardUrl("http://localhost:4444/")
                    .assertDemoProjectIsShown();
    }
}
