import Config.Config;
import org.testng.annotations.Test;
import pages.LoginPage;

import java.util.UUID;

public class Test9_Correct_add_Characteristic_show_in_characteristic_table extends SeleniumBaseTest {
    @Test
    public void addCharacteristic(){
        String processName = "DEMO PROJECT";
        String characteristicName = UUID.randomUUID().toString().substring(0, 10);
        String lsl = "4";
        String usl = "8";

        new LoginPage(driver)
                .typeEmail(new Config().getApplicationUser())
                .typePassword(new Config().getApplicationPassword())
                .submitLogin()
                    .goToCharacteristics()
                    .clickAddCharacteristic()
                    .selectProcess(processName)
                    .typeName(characteristicName)
                    .typeLsl(lsl)
                    .typeUsl(usl)
                    .submitCreate()
                        .assertCharacteristic(characteristicName, lsl, usl, "");
    }
}

