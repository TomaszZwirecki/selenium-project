import Config.Config;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.RegisterPage;

public class Test3_incorrect_user_register_with_dataprovider extends SeleniumBaseTest {
    @DataProvider
    public Object[][] getWrongPassword() {
        return new Object[][]{
                {"Test1", "The Password must be at least 6 and at max 100 characters long."},
                {"Test11", "Passwords must have at least one non alphanumeric character."},
                {"test1!", "Passwords must have at least one uppercase ('A'-'Z')."}
        };
    }
    @Test(dataProvider = "getWrongPassword")
    public void registrationPasswordFormatErrorTest(String password, String expectedError) {
        new RegisterPage(driver)
                .gotoRegisterPage()
                .typeEmail(new Config().getApplicationUser())
                .typePassword(password)
                .typePassword2(password)
                .submitRegisterWithFailure()
                .assertList(expectedError);
    }
}
