import Config.Config;
import pages.LoginPage;
import org.testng.annotations.Test;



public class Test6_Correct_add_process extends SeleniumBaseTest{
    @Test
    public void addProcessTest(){
        String processName = "Test procesu";

        new LoginPage(driver)
                .typeEmail(new Config().getApplicationUser())
                .typePassword(new Config().getApplicationPassword())
                .submitLogin()
                .goToProcesses()
                .clickAddProcess()
                .typeName(processName)
                .submitCreate()
                .assertProcess(processName,"","");
    }
}

