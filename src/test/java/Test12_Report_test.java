import Config.Config;
import org.testng.annotations.Test;
import pages.LoginPage;

import java.util.UUID;

public class Test12_Report_test extends SeleniumBaseTest {
    @Test
    public void reportTest(){
        String processName = "DEMO PROJECT";
        String characteristicName = UUID.randomUUID().toString().substring(0, 10);
        String lsl = "8";
        String usl = "10";
        String sampleName = "Test sample";
        String results = "8.0;9.0";
        String expMean = "8.5000";
        String standardDeviation = "0.7071";

        new LoginPage(driver)
                .typeEmail(new Config().getApplicationUser())
                .typePassword(new Config().getApplicationPassword())
                .submitLogin()
                .goToCharacteristics()
                    .clickAddCharacteristic()
                    .typeName(characteristicName)
                    .typeLsl(lsl)
                    .typeUsl(usl)
                    .selectProcess(processName)
                    .submitCreate()
                .goToResults(characteristicName)
                    .clickAddResults()
                    .typeResults(results)
                    .typeSampleName(sampleName)
                    .submitCreate()
                    .backToCharacteristics()
                .goToReport(characteristicName)
                    .assertMean(expMean)
                    .assertLSL(lsl)
                    .assertUSL(usl)
                    .assertStandardDevaiation(standardDeviation);

    }
}

