import Config.Config;
import org.testng.annotations.Test;
import pages.RegisterPage;

public class Test1_Correct_user_register extends SeleniumBaseTest {
    @Test
    public void correctRegisterTest() {

        new RegisterPage(driver)
                .gotoRegisterPage()
                .typeRandomEmail()
                .typePassword(new Config().getApplicationPassword())
                .typePassword2(new Config().getApplicationPassword())
                .submitRegister()
                .assertWelcomeElementIsShow();
    }
}

