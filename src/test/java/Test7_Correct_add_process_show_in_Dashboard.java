import Config.Config;
import org.testng.annotations.Test;
import pages.LoginPage;

public class Test7_Correct_add_process_show_in_Dashboard extends SeleniumBaseTest {
    @Test
    public void addProcessWithDashboardTest() {
        String processName = "Test procesu";

        new LoginPage(driver)
                .typeEmail(new Config().getApplicationUser())
                .typePassword(new Config().getApplicationPassword())
                .submitLogin()
                .goToProcesses()
                    .clickAddProcess()
                    .typeName(processName)
                    .submitCreate()
                .goToDashboard()
                    .assertTestProjectIsShown();
    }
}
