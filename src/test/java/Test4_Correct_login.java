import Config.Config;
import pages.LoginPage;
import org.testng.annotations.Test;


public class Test4_Correct_login extends SeleniumBaseTest {


    @Test
    public void correctLoginTest() {
        new LoginPage(driver)
                .typeEmail(new Config().getApplicationUser())
                .typePassword(new Config().getApplicationPassword())
                .submitLogin()
                .assertWelcomeElementIsShown();

    }
}
