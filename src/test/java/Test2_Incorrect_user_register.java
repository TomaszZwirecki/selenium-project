
import Config.Config;
import pages.RegisterPage;
import org.testng.annotations.Test;


public class Test2_Incorrect_user_register extends SeleniumBaseTest {

    @Test
    public void incorrectRegisterTestWrongPassword() {


        new RegisterPage(driver)
                .gotoRegisterPage()
                .typeEmail(new Config().getApplicationUser())
                .typePassword("Test1!")
                .typePassword2("Test2!")
                .submitRegister()
                .assertErrorElmIsShow();
    }
}
