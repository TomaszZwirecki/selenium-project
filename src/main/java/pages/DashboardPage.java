package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class DashboardPage extends HomePage {

    public DashboardPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//h2[text()='DEMO PROJECT']")
    private WebElement demoProjectHeader;

    @FindBy(linkText = "Create your first process")
    private WebElement createFirstProjectBtn;

    @FindBy(xpath = "//h2[text()='Test procesu']")
    private WebElement demoTestHeader;


    public DashboardPage assertDashboardUrl(String pageUrl) {
        Assert.assertEquals(driver.getCurrentUrl(), pageUrl);

        return this;
    }

    public DashboardPage assertDemoProjectIsShown() {
        Assert.assertTrue(isElementPresent(demoProjectHeader) || isElementPresent(createFirstProjectBtn));

        return this;
    }

    public DashboardPage assertTestProjectIsShown() {
        Assert.assertTrue(isElementPresent(demoTestHeader));

        return this;
    }

}
