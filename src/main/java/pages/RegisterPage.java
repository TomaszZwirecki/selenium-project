package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import java.util.List;
import java.util.UUID;

public class RegisterPage {
    protected WebDriver driver;

    public RegisterPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    String randomEmail() {
        return "Random-" + UUID.randomUUID().toString() + "@example.com";
    }

    @FindBy(id = "Email")
    private WebElement registerEmailTxt;

    @FindBy(css = "#Password")
    private WebElement registerPasswordTxt;

    @FindBy(css = "#ConfirmPassword")
    private WebElement registerComfirmTxt;

    @FindBy(css = "button[type=submit]")
    private WebElement registerBtn;

    @FindBy(css = ".profile_info>h2")
    private WebElement welcomeElm;

    @FindBy(css = "#ConfirmPassword-error")
    private WebElement errorElm;

    @FindBy(css = "a[href*=Register")
    private WebElement registerLnk;

    @FindBy(css = ".validation-summary-errors>ul>li")
    public List<WebElement> loginErrors;


    public RegisterPage typeEmail(String email) {
        registerEmailTxt.clear();
        registerEmailTxt.sendKeys(email);
        return this;
    }

    public RegisterPage typeRandomEmail() {
        registerEmailTxt.clear();
        registerEmailTxt.sendKeys(randomEmail());
        return this;
    }

    public RegisterPage typePassword(String password) {
        registerPasswordTxt.clear();
        registerPasswordTxt.sendKeys(password);
        return this;
    }

    public RegisterPage typePassword2(String password) {
        registerComfirmTxt.clear();
        registerComfirmTxt.sendKeys(password);
        return this;
    }

    public RegisterPage submitRegister() {
        registerBtn.click();
        return this;
    }

    public RegisterPage submitRegisterWithFailure() {
        registerBtn.click();

        return this;
    }


    public RegisterPage gotoRegisterPage() {
        registerLnk.click();
        return this;
    }

    public RegisterPage assertList(String expected) {
        boolean errorExists = false;

        errorExists = loginErrors
                .stream()
                .anyMatch(loginError -> loginError.getText().equals(expected));
        Assert.assertTrue(errorExists);

        return this;
    }

    public RegisterPage assertWelcomeElementIsShow() {
        Assert.assertTrue(welcomeElm.isDisplayed(), "Welcome element is not shown.");
        Assert.assertTrue(welcomeElm.getText().contains("Welcome"), "Welcome element text: '" + welcomeElm.getText() + "' does not contain word 'Welcome'");

        return this;
    }

    public RegisterPage assertErrorElmIsShow() {
        Assert.assertTrue(errorElm.getText().contains("The password and confirmation password do not match"));
        return this;
    }
}


