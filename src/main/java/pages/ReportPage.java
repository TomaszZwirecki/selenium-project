package pages;

import junit.framework.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ReportPage {
    protected WebDriver driver;

    public ReportPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//td[text()='Mean (x)']/../td[2]")
    private WebElement meanCell;

    @FindBy(xpath = "//td[text()='Lower Specification Limit (LSL)']/../td[2]")
    private WebElement LSLCell;

    @FindBy(xpath = "//td[text()='Upper Specification Limit (USL)']/../td[2]")
    private WebElement USLCell;

    @FindBy(xpath = "//td[text()='Standard deviation (s)']/../td[2]")
    private WebElement SDCell;


    public ReportPage assertMean(String expMean) {
        Assert.assertEquals(meanCell.getText(), expMean);

        return this;
    }

    public ReportPage assertLSL(String lsl) {
        float lsl1 = Float.parseFloat(lsl) + (0.0000F);
        float liczbaLSL = Float.parseFloat(LSLCell.getText());
        String liczbaLSLString = String.valueOf(liczbaLSL);
        String LSLString = String.valueOf(lsl1);
        Assert.assertEquals(LSLString, liczbaLSLString);
        return this;
    }

    public ReportPage assertUSL(String usl) {
        float usl1 = Float.parseFloat(usl) + (0.0000F);
        float liczbaUSL = Float.parseFloat(USLCell.getText());
        String liczbaUSLString = String.valueOf(liczbaUSL);
        String USLString = String.valueOf(usl1);
        Assert.assertEquals(USLString, liczbaUSLString);
        return this;
    }

    public ReportPage assertStandardDevaiation(String sd) {
        float sd1 = Float.parseFloat(sd) + (0.0000F);
        float liczbaSD = Float.parseFloat(SDCell.getText());
        String liczbaSDString = String.valueOf(liczbaSD);
        String SDString = String.valueOf(sd1);
        Assert.assertEquals(SDString, liczbaSDString);
        return this;
    }
}
